import React from 'react';
import {newBlog} from '../api';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import {toast} from 'react-toastify';
import Loader from './loader';

class Addblog extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			title: "",
			desc: "",
			showLoader: false,
		};
	}

	submit = async (e) => {
		e.preventDefault();
		const {title, desc} = this.state;
		if(title === '' || desc === '') {
			toast.info('Please Enter All Fields!');
		}
		else {
			this.setState({showLoader: true});
			await newBlog({title: this.state.title, desc: this.state.desc});
		    this.setState(
		        {
		            title: '',
					desc: '',
					showLoader: false
		        }
		    )
		    toast.success('Blog Added Successfully!')
		    this.props.history.push('/')
		}

	}

	handleChange = (e) => {
		this.setState({[e.target.name]: e.target.value})
	}

	render() {
		if(!this.props.isLoggedIn) {
			return <Redirect to="/user/login" />
		}
		return(
			<React.Fragment>
			<br />
			<div className="row">
				<form className="container col-11 col-sm-8 col-md-7 px-2 py-3 px-md-4" style={{margin: "20px auto 30px auto", boxShadow: "5px 5px 20px", borderRadius: "10px"}} >
					<div className="form-group">
						<label htmlFor="title">Enter Blog Title :</label>
						<input type="text" className="form-control" value={this.state.title} onChange={this.handleChange} name="title" id="title" placeholder="Title of blog..." />
						</div>
					<div className="form-group">
						<label htmlFor="desc">Enter Description of Blog :</label>
						<textarea className="form-control" value={this.state.desc} onChange={this.handleChange} name="desc" id="desc" rows="5" placeholder="Description of blog...">
						</textarea>
					</div>
					<button onClick={this.submit} className="btn btn-success" style={{width: "100%"}} >Submit Blog</button>
				</form>
			</div>
			<Loader
				showLoader={this.state.showLoader}				
			/>
			</React.Fragment>
		)
	}
}

const mapStateToProps = state => ({
	isLoggedIn: state.isLoggedIn
})

export default connect(mapStateToProps)(Addblog);