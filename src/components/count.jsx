import React from 'react';
import { connect } from 'react-redux';
import { increment, decrement } from '../redux/actions/actions';

class Count extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            disabled: "disabled",
        }
    }

    checkCondition = () => {
        setTimeout(() => {
            if(this.props.counter === 0)
                this.setState({disabled: "disabled"});
            else
                this.setState({disabled: ""});
        },20)
    }

    increment = () => {
        this.props.increment();
        // this.setState(prevState => ({count : prevState.count + 1}));
        this.checkCondition();
    }

    decrement = () => {
        this.props.decrement();
        // if(this.state.count > 0)
        //     this.setState(prevState => ({count : prevState.count - 1}));
        this.checkCondition();
    }

	render() {
		return(
			<React.Fragment>
				<center>
                    <h1>
                        Increment and Decrement using Redux
                    </h1>
                    <br />
					<h2>
                        Count is {this.props.counter}
					</h2>
                    <br />
                    <br />
                    <button className="btn btn-success" onClick={this.increment} >Increment</button>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <button className="btn btn-danger" disabled={this.state.disabled} onClick={this.decrement} >Decrement</button>
				</center>
			</React.Fragment>
		)
	}
}

const mapStateToProps = state => ({
    counter: state.counter
})

const mapDispatchToProps = (dispatch) => {
    return {
      increment: () => {dispatch(increment())},
      decrement: () => {dispatch(decrement())}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Count);