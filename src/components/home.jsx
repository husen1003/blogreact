import React from 'react';
import { getBlogs } from '../api';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Loader from './loader'

class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			blogs: [],
			showLoader: false
		};
	}

componentDidMount() {
	this.gettingBlogs();
}

gettingBlogs = async () => {
	this.setState({showLoader: true});
	const res = await getBlogs();
	this.setState({blogs: res.data, showLoader:false});
}


	render() {
		if(!this.props.isLoggedIn) {
			return <Redirect to="/user/login" />
		}
		return(
			<React.Fragment>				{
					this.state.blogs.map((blog, id) => {
						return(
							<div key={id} className="jumbotron jumbotron-fluid" style={{background: '#e6ffe6'}} >
							  <div className="container">
							    <h1 className="display-4">{blog.title}</h1><br />
							    <p className="lead">{blog.desc}</p>
							  </div>
							</div>
						)
					})
				}
			<Loader
				showLoader={this.state.showLoader}				
			/>
			</React.Fragment>
		)
	}
}

const mapStateToProps = state => ({
	isLoggedIn: state.isLoggedIn
})

export default connect(mapStateToProps)(Home);