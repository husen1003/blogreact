import React from 'react';
import { getBlog, editBlog } from '../api';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';
import Loader from './loader';

class Updateblog extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			title: null,
			desc: null,
			flag: false,
			showLoader: false
		};
	}

	id = this.props.match.params.id;

	componentDidMount() {
		this.getBlogToState();
	}

	getBlogToState = async () => {
		this.setState({showLoader: true});
		const res = await getBlog(this.id);
		this.setState({
			title: res.data[0].title,
			desc: res.data[0].desc,
			flag: true,
			showLoader: false
		});
	}

	handleChange = (e) => {
		this.setState({[e.target.name]: e.target.value})
	}

	submit = async (e) => {
		e.preventDefault();
		const {title, desc} = this.state;
		if(title === '' || desc === '') {
			toast.info('Please Enter All Fields!')
		}
		else {
			const data = {
				title: this.state.title,
				desc: this.state.desc
			}
			this.setState({showLoader: true});
			await editBlog(data, this.id);
			this.setState({showLoader: false});
			toast.success('Bload Updated Successfully!')
		    this.props.history.push(`/update-delete/`)
		}
	}
	render() {
		if(!this.props.isLoggedIn) {
			return <Redirect to="/user/login" />
		}
		return(
			<React.Fragment>
			{	this.state.flag ?
				<div className="row">
					<form className="container col-11 col-sm-8 col-md-7 px-2 py-3 px-md-4" style={{margin: "40px auto 30px auto", borderRadius: "10px", boxShadow: "5px 5px 20px"}} >
						<div className="form-group">
							<label htmlFor="title">Enter Blog Title :</label>
							<input type="text" className="form-control" value={this.state.title} onChange={this.handleChange} name="title" id="title" placeholder="Title of blog..." />
						</div>
						<div className="form-group">
							<label htmlFor="desc">Enter Description of Blog :</label>
							<textarea className="form-control" value={this.state.desc} onChange={this.handleChange} name="desc" id="desc" rows="5" placeholder="Description of blog...">
							</textarea>
						</div>
						<button onClick={this.submit} className="btn btn-success" style={{width: "100%"}} >Update Blog</button>
					</form>
				</div>
				: null
			}
			<Loader
				showLoader={this.state.showLoader}				
			/>
			</React.Fragment>
		)
	}
}

const mapStateToProps = state => ({
	isLoggedIn: state.isLoggedIn
})

export default connect(mapStateToProps)(Updateblog);