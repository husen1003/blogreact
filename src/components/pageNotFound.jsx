import React from 'react';

class PageNotFound extends React.Component {
	render() {
		return(
			<React.Fragment>
				<center>
					<h1>
						Oops, Sorry! Page Not Found ...
					</h1>
				</center>
			</React.Fragment>
		)
	}
}

export default PageNotFound;