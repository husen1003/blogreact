import React from 'react'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
// import { userAuth } from '../../api';
import Loader from '../loader'

class Profile extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showLoader: false,
            name: this.props.user.name,
            lname: this.props.user.lname,
            mo: this.props.user.mo,
            disabled: "disabled",
            isUpdating : false,
            display: "none"
        }
    }

    // async componentDidMount() {
    //     let res = await userAuth({token: localStorage.getItem('token')});
    //     // this.setState({showLoader: false, name: res.data.user.name, lname: res.data.user.lname, mo: res.data.user.mo});
    // }


    handleChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    submit = e => {
        e.preventDefault();
        if(this.state.isUpdating){
            console.log("isUpdating");
        }
        else {
            this.setState({disabled: "", isUpdating: true, display: "block"})
        }
    }

    cancel = e => {
        e.preventDefault();
        this.setState({disabled: "disabled", isUpdating: false, display: "none"});
    }

    render() {
		if(!this.props.isLoggedIn) {
			return <Redirect to="/user/login" />
		}
        return (
			<React.Fragment>
				<div className="row" style={{transition: "all 1s ease-out"}}>
					<form className="container col-11 col-sm-8 col-md-7 px-2 py-3 px-md-4" style={{margin: "60px auto", borderRadius: "10px", boxShadow: "5px 5px 20px"}} >
						<div className="form-group">
							<label htmlFor="name">Name :</label>
							<input type="text" className="form-control" value={this.state.name} disabled={this.state.disabled} onChange={this.handleChange} name="name" id="name" placeholder="Enter Name" />
						</div>
						<div className="form-group">
							<label htmlFor="lname">Lastname :</label>
							<input type="text" className="form-control" value={this.state.lname} disabled={this.state.disabled} onChange={this.handleChange} name="lname" id="lname" placeholder="Enter Lastname" />
						</div>
						<div className="form-group">
							<label htmlFor="mo">Mobile :</label>
							<input type="text" className="form-control" value={this.state.mo} disabled={this.state.disabled} onChange={this.handleChange} name="mo" id="mo" placeholder="Mobile Number" />
						</div>
                        <div style={{display: "flex"}}>
                            <div style={{flex: 1, display: this.state.display}}>
                                <button onClick={this.cancel} className="btn btn-success" style={{width: "100%", marginTop: "10px"}} >Cancel</button>
                            </div>
                            <div style={{flex: 1}}>
                                <button onClick={this.submit} className="btn btn-success" style={{width: "100%", marginTop: "10px"}} >Update</button>
                            </div>
                        </div>
					</form>
				</div>
				<Loader
					showLoader={this.state.showLoader}				
				/>
			</React.Fragment>
        )
    }

}

const mapStateToProps = state => ({
    isLoggedIn: state.isLoggedIn,
    user: state.user.user,
})

export default connect(mapStateToProps)(Profile);