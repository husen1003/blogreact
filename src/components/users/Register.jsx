import React from 'react';
import { newUser } from '../../api';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import Loader from '../loader';

class Register extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			fname: "",
			lname: "",
			mo: "",
			email: "",
			pass: "",
			cpass: "",
			showLoader: false
		};
	}

	handleChange = (e) => {
		this.setState({[e.target.name]: e.target.value})
	}
	submit = async (e) => {
		e.preventDefault();
		const {fname, lname, mo, email, pass, cpass} = this.state;
		if(fname==="" || lname==="" || mo==="" || email==="" || pass==="" || cpass==="" ){
			toast.info('Please Enter All Fields');
		}
		else{
			if(pass === cpass) {
				this.setState({showLoader: true});
				const res = await newUser(this.state);
				if(!res.data.success && res.data.emailExists)
					toast.info('Email Id already registered!')
				if(!res.data.success && res.data.mobileExists)
					toast.info('Mobile number is already registered!')
				if(res.data.success){
					toast.success('Activation link has sent to your email, please verify your email id!')
				    this.props.history.push('/login')
				}
				this.setState({showLoader: false});
		}
			else{
				toast.info('Both password must be same!');
			}
		}
	}

	render() {
		if(this.props.isLoggedIn) {
			return <Redirect to="/user/" />
		}
		return(
			<React.Fragment>
			<div className="row">
				<form className="container col-11 col-sm-8 col-md-7 px-2 py-3 px-md-4" style={{margin: "60px auto", boxShadow: "5px 5px 20px", borderRadius: "10px"}} >
					<div className="form-group">
						<label htmlFor="title">First name :</label>
						<input type="text" className="form-control" value={this.state.fname} onChange={this.handleChange} name="fname" id="fname" placeholder="Enter first name..." />
					</div>
					<div className="form-group">
						<label htmlFor="desc">Last name :</label>
						<input type="text" className="form-control" value={this.state.lname} onChange={this.handleChange} name="lname" id="lname" rows="6" placeholder="Enter last name..." />
					</div>
					<div className="form-group">
						<label htmlFor="mo">Mobile no :</label>
						<input type="text" className="form-control" value={this.state.mo} onChange={this.handleChange} name="mo" id="mo" placeholder="Enter Mobile number..." />
					</div>
					<div className="form-group">
						<label htmlFor="email">Email :</label>
						<input type="text" className="form-control" value={this.state.email} onChange={this.handleChange} name="email" id="email" rows="6" placeholder="Enter Email address..." />
					</div>
					<div className="form-group">
						<label htmlFor="pass">Password :</label>
						<input type="password" className="form-control" value={this.state.pass} onChange={this.handleChange} name="pass" id="pass" placeholder="Enter your password..." />
					</div>
					<div className="form-group">
						<label htmlFor="cpass">Confirm Password :</label>
						<input type="password" className="form-control" value={this.state.cpass} onChange={this.handleChange} name="cpass" id="cpass" placeholder="Confirm your password..." />
					</div>
					<button onClick={this.submit} className="btn btn-success" style={{width: "100%"}} >Submit</button>
				</form>
			</div>
			<Loader
				showLoader={this.state.showLoader}				
			/>
			</React.Fragment>
		)
	}
}

const mapStateToProps = state => ({
	isLoggedIn: state.isLoggedIn
})
export default connect(mapStateToProps)(Register);