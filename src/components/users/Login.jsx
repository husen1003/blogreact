import React from 'react';
import { userLogin, userVerify, decodeToken } from '../../api';
import { changeLogin, adminLogin } from '../../redux/actions/actions'
import { Redirect, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import Loader from '../loader'

class Login extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			email: "",
			pass: "",
			disabled: "",
			showLoader: false
		}
	}

	async componentDidMount() {
		if(this.token) {
			this.setState({showLoader: true});
			let res = await decodeToken({token: this.token});
			(res.data.success && res.data.email) ? this.setState({email: res.data.email, disabled: "disabled", showLoader: false}) : this.inValidToken();
		}
	}

	token = this.props.match.params.token;

	handleChange = (e) => {
		this.setState({[e.target.name]: e.target.value})
	}

	inValidToken = () => {
		toast.error('Invalid Token!');
		this.props.history.push('/login')
	}

	loginUser = async () => {
		const data = {
			email: this.state.email,
			pass: this.state.pass
		}
		this.setState({showLoader: true});
		let res = await userLogin(data);
		this.setState({showLoader: false});

		// if email id is not exists
		if(res.data.type){
			this.props.changeAdminLogin();
		}
		else{
			if(res.data.emailNotExists) {
				toast.info('Email Id or Mobile number is not Registered')
			}
			else {
	
				// checking that user is virified or not
				if(res.data.isNotVerified){
					toast.info('Your account is not activated, please verify! we have sent verification link');
				}
				else {
					// storing token and login the user
					if(res.data.success) {
						localStorage.setItem("token", res.data.token);
						this.props.changeLoginStatus(res.data);
						toast.success('LoggedIn Successfully!')
					}
					else {
						toast.info('Invalid Email Id or Password!')
					}
				}
			}
		}
	}

	submit = async (e) => {
		e.preventDefault();
		const {email, pass} = this.state;

		// if email and password field is empty
		if(email==='' || pass==='') {
			toast.info('Please Enter All Fields!')
		}
		else {
			// * if user logged in first time and redirected from activation link by email
			if(this.token){

				const data = {
					email: this.state.email,
					pass: this.state.pass
				}
				let loginResult = await userLogin(data);

				// if email id is not exists
				if(loginResult.data.emailNotExists) {
					toast.info('Email Id or Mobile number is not Registered')
				}
				else {
					// storing token and login the user
					if(loginResult.data.success) {

						// updating and virifying user
						let res = await userVerify({token: this.token});
						localStorage.setItem("token", res.data.token);
						this.props.changeLoginStatus(res.data);
						toast.success('LoggedIn Successfully!')
					}
					else {
						toast.info('Invalid Email Id or Password!')
					}
				}
			}
			else{
				this.loginUser();
			}
		}

	}
	render() {
		if(this.props.isLoggedIn) {
			return <Redirect to="/user" />
		}
		if(this.props.isAdminLoggedIn) {
			return <Redirect to="/admin" />
		}
		return(
			<>
				<div className="row">
					<form className="container col-11 col-sm-8 col-md-7 px-2 py-3 px-md-4" style={{margin: "60px auto", borderRadius: "10px", boxShadow: "5px 5px 20px"}} >
						<div className="form-group">
							<label htmlFor="email">Enter Email Id or Mobile :</label>
							<input type="text" className="form-control" value={this.state.email} disabled={this.state.disabled} onChange={this.handleChange} name="email" id="email" placeholder="Email Id or Mobile number" />
						</div>
						<div className="form-group">
							<label htmlFor="pass">Password :</label>
							<input type="password" className="form-control" value={this.state.pass} onChange={this.handleChange} name="pass" id="pass" placeholder="Enter password..." />
						</div>
						<span>
							<Link to={`/user/forgotpassword/${this.state.email}`}>
								Forgot password
							</Link>
						</span>
						<button onClick={this.submit} className="btn btn-success" style={{width: "100%", marginTop: "10px"}} >Login</button>
					</form>
				</div>
				<Loader
					showLoader={this.state.showLoader}				
				/>
			</>
		)
	}
}

const mapStateToProps = state => ({
	isLoggedIn: state.isLoggedIn,
	isAdminLoggedIn: state.isAdminLoggedIn
})

const mapDispatchToProps = (dispatch) => {
    return {
	  changeLoginStatus: (name) => {dispatch(changeLogin(name))},
	  changeAdminLogin: () => {dispatch(adminLogin())}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);