import React from 'react';
import { Redirect } from 'react-router-dom';
import { resetPass, decodeToken } from '../../api';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import Loader from '../loader'

class ResetPassword extends React.Component {

	token = this.props.match.params.token;
	constructor(props) {
		super(props);
		this.state = {
			pass: "",
			cpass: "",
			showLoader: true
		};
	}

	async componentDidMount() {
		if(this.token){
			let res = await decodeToken({token: this.token});
			this.setState({showLoader: false});
			if(!res.data.success && !res.data.id)
				this.inValidToken();
		}
	}

	inValidToken = () => {
		toast.error('Invalid Token!');
		this.props.history.push('/login')
	}

	handleChange = (e) => {
		this.setState({[e.target.name]: e.target.value})
	}

	submit = async (e) => {
		e.preventDefault();
		if(this.state.pass==="" || this.state.cpass===""){
			toast.info('Please Enter Both Password!');
		}
		else {
			if(this.state.pass===this.state.cpass){
				this.setState({showLoader: true});
				const res = await resetPass({token: this.token, pass: this.state.pass});
				this.setState({showLoader: false});
				if(res.data.success) {
					toast.info('Password has been changed successfully!');
				    this.props.history.push('/login')
				}
			}
			else{
				toast.error('Both Password must be same!')
			}
		}
	}

	render() {
		if(this.props.isLoggedIn) {
			return <Redirect to="/user" />
		}
		return(
			<React.Fragment>
				<br />
				<div className="row">
					<form className="container col-11 col-sm-8 col-md-7 px-2 py-3 px-md-4" style={{margin:"20px auto 50px auto", boxShadow: "5px 5px 20px", borderRadius: "10px"}} >
						<div className="form-group">
							<label htmlFor="pass">Password :</label>
							<input type="password" className="form-control" value={this.state.pass} onChange={this.handleChange} name="pass" id="pass" placeholder="Enter Password" />
						</div>
						<div className="form-group">
							<label htmlFor="cpass">Confirm Password :</label>
							<input type="password" className="form-control" value={this.state.cpass} onChange={this.handleChange} name="cpass" id="cpass" placeholder="Confirm Password" />
						</div>
						<button onClick={this.submit} className="btn btn-success" style={{width: "100%"}} >Reset Password</button>
					</form>
				</div>
				<Loader
					showLoader={this.state.showLoader}				
				/>
			</React.Fragment>
		)
	}
}

const mapStateToProps = state => ({
	isLoggedIn: state.isLoggedIn
})
export default connect(mapStateToProps)(ResetPassword);