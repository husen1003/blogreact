import React from 'react';
import { Redirect } from 'react-router-dom';
import { forgotPass } from '../../api';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import Loader from '../loader';

class ForgotPassword extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			email: "",
			showLoader: false
		};
	}

	componentDidMount(){
		if(this.email)
			this.setState({email: this.email});
	}

	email = this.props.match.params.email;

	handleChange = (e) => {
		this.setState({[e.target.name]: e.target.value})
	}
	submit = async (e) => {
		e.preventDefault();
		if(this.state.email===""){
			toast.info('Enter Email ID !');
		}
		else {
			this.setState({showLoader: true});
			const res = await forgotPass({email: this.state.email});
			this.setState({showLoader: false});
			if(!res.data.emailNotExists){
				toast.success('Password Reset link has been sent to your email! Please check it and verify!')
			}
			else{
				toast.error('Email does not exists!')
			}
		}
	}

	render() {
		if(this.props.isLoggedIn) {
			return <Redirect to="/user" />
		}
		return(
			<React.Fragment>
				<br />
				<div className="row">
					<form className="container col-11 col-sm-8 col-md-7 px-2 py-3 px-md-4" style={{margin:"20px auto 50px auto", boxShadow: "5px 5px 20px", borderRadius: "10px"}} >
						<div className="form-group">
							<label htmlFor="email">Email :</label>
							<input type="text" className="form-control" value={this.state.email} onChange={this.handleChange} name="email" id="email" placeholder="Enter Email address..." />
						</div>
						<button onClick={this.submit} className="btn btn-success" style={{width: "100%"}} >Get Password Reset Link</button>
					</form>
				</div>
				<Loader
					showLoader={this.state.showLoader}				
				/>
			</React.Fragment>
		)
	}
}

const mapStateToProps = state => ({
	isLoggedIn: state.isLoggedIn
})
export default connect(mapStateToProps)(ForgotPassword);