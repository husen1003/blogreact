import React, { Component } from 'react';
import { deleteBlog, editBlog, getBlogs, newBlog } from '../../api'
import Loader from '../loader'
import { Modal, Table, Space, Button } from 'antd';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';

class Blogs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            blogs: null,
            showLoader: false,
            isModalVisible: false,
            modalBlogId: null,
            modalTitile: null,
            modalDesc: null,
            blogType: null,
        }
    }

    componentDidMount() {
        this.getAllBlogs();
    }

    columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            render: (text, record, key) => <Link to="#" key={key} >{text}</Link>,
        },
        {
            title: 'Description',
            dataIndex: 'desc',
            key: 'desc',
            render: (text, record, key) => (<div key={key}>{text}</div>)
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record, key) => (
                <Space size="middle">
                    <Link to="#" key={key} onClick={() => {
                        this.setState({isModalVisible: true, blogType: "update", modalBlogId: record._id, modalTitile: record.title, modalDesc: record.desc});
                    }} >Edit</Link>
                    <Link to="#" key={key} onClick={async () => {
                        this.setState({ showLoader: true });
                        await deleteBlog(record._id);
                        const res = await getBlogs();
                        this.setState({ blogs: res.data, showLoader: false });
                        toast.error('Blog Deleted!');
                    }} >Delete</Link>
                </Space>
            ),
        },
    ];

    getAllBlogs = async () => {
        this.setState({ showLoader: true });
        const res = await getBlogs();
        this.setState({ blogs: res.data, showLoader: false });
    }

    handleOk = async (e) => {
        
		const {modalTitile, modalDesc} = this.state;
		if(modalTitile === '' || modalDesc === '') {
			toast.info('Please Enter All Fields!');
		}
		else {
            this.setState({showLoader: true});
            if(this.state.blogType === "new"){
                await newBlog({title: modalTitile, desc: modalDesc});
                const res = await getBlogs();
                this.setState(
                    {
                        blogs: res.data,
                    }
                )
                toast.success('Blog Added Successfully!')
            }
            if(this.state.blogType === "update"){
                const data = {
                    title: this.state.modalTitile,
                    desc: this.state.modalDesc
                }
                await editBlog(data, this.state.modalBlogId);
                const res = await getBlogs();
                this.setState({
                    blogs: res.data,                  
                });
                toast.success('Bload Updated Successfully!')
            }
            this.setState({
                    modalTitile: null,
                    modalDesc: null,
                    modalBlogId: null,
                    showLoader: false,
                    isModalVisible: false,
                }
            )
		}
    };
    handleCancel = () => {
        this.setState({
            isModalVisible: false,
            modalTitile: null,
            modalDesc: null,
            modalBlogId: null,
        });
    }
    handleChange = (e) => {
		this.setState({[e.target.name]: e.target.value})
    }

    render() {
        return (
            <>
                <Modal
                    title="Basic Modal"
                    visible={this.state.isModalVisible}
                    onOk = {this.handleOk}
                    onCancel = {this.handleCancel}
                    footer={[
                        <Button key="back" onClick={this.handleCancel}>
                            Cancel
                        </Button>,
                        <Button key="submit" loading={this.state.loading} type="primary" onClick={() => {
                            this.handleOk()
                        }}>
                            {(this.state.blogType === "new") ? "Add" : "Update"}
                        </Button>,
                    ]}
                >
                    <form>
                        <div className="form-group">
                            <label htmlFor="modalTitile">Enter Blog Title :</label>
                            <input type="text" className="form-control" value={this.state.modalTitile} onChange={this.handleChange} name="modalTitile" id="modalTitile" placeholder="Title of blog..." />
                        </div>
                        <div className="form-group">
                            <label htmlFor="modalDesc">Enter Description of Blog :</label>
                            <textarea className="form-control" value={this.state.modalDesc} onChange={this.handleChange} name="modalDesc" id="modalDesc" rows="5" placeholder="Description of blog...">
                            </textarea>
                        </div>
                    </form>
                </Modal>
                <Table columns={this.columns} dataSource={this.state.blogs} />
                <Loader
                    showLoader={this.state.showLoader}
                />
                <br />
                <center>
                    <Button type="primary" onClick={() => {
                        this.setState({ isModalVisible: true, blogType: "new", modalBlogId: null, modalTitile: null, modalDesc: null });
                    }} >
                        Add Blog
                    </Button>
                </center>
            </>
        );
    }
}

export default Blogs;