import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { adminLogout } from '../../redux/actions/actions';
import Loader from '../loader';

class HeaderAdmin extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showLoader: false
		};
	}
	render() {
		if(!this.props.isAdminLoggedIn) {
			return <Redirect to="/user/login" />
		}
		return(
			<React.Fragment>
			<ul className="nav nav-pills" style={{background: "#99ff99", color: "white"}}>
				<li className="nav-item">
					<Link className="nav-link" style={{color: "black", border:"1px solid green"}} to="/admin">DashBoard</Link>
				</li>
				<li className="nav-item">
					<Link className="nav-link" style={{color: "black", border:"1px solid green"}} to="/admin/blogs">Show Blogs</Link>
				</li>
				<li className="nav-item">
					<Link className="nav-link" style={{color: "black", border:"1px solid green"}} to="#" onClick={() => {
						this.props.logOut();
					}} >Logout</Link>
				</li>
			</ul>
			<Loader
				showLoader={this.state.showLoader}				
			/>
			</React.Fragment>
		)
	}
}

const mapStateToProps = state => ({
	isAdminLoggedIn: state.isAdminLoggedIn
})

const mapDispatchToProps = (dispatch) => {
    return {
	  logOut: () => {dispatch(adminLogout())}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderAdmin);