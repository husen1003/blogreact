import React, { Component } from 'react';
import { getUsers, changeVerified } from '../../api'
import Loader from '../loader'
import { Modal, Button, Table, Space, Switch } from 'antd';
import { Link } from 'react-router-dom';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: null,
            showLoader: false,
            isModalVisible: false,
            modalName: null,
            modalLname: null,
            modalMo: null,
            modalEmail: null,
        }
    }

    componentDidMount() {
        this.getAllUsers();
    }

    columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: (text, record, i) => <Link to="#" key={i}>{text}</Link>,
        },
        {
            title: 'Mobile',
            dataIndex: 'mo',
            key: 'mo',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Verified',
            dataIndex: 'verified',
            key: 'verified',
            render: (data, record) => {
                if (data)
                    return (<Switch defaultChecked onChange={async (value) => {
                        console.log(value)
                        console.log(record.id)
                        this.setState({ showLoader: true });
                        await changeVerified({ id: record.id, status: value });
                        this.setState({ showLoader: false });
                    }} />);
                else
                    return (<Switch onChange={async (value) => {
                        console.log(value)
                        console.log(record.id)
                        this.setState({ showLoader: true });
                        await changeVerified({ id: record.id, status: value });
                        this.setState({ showLoader: false });
                    }} />);
            }
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record, key) => (
                <Space size="middle" key={key}>
                    <Link to="#" onClick={() => {
                        this.setState({modalName: record.fname, modalLname: record.lname, modalMo: record.mo, modalEmail: record.email, isModalVisible: true});
                    }}>Edit</Link>
                    <Link to="#">Delete</Link>
                </Space>
            ),
        },
    ];

    handleOk = () => {
        this.setState({isModalVisible: false});
    };
    handleCancel = () => {
        this.setState({isModalVisible: false});
    }

    handleChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    getAllUsers = async () => {
        this.setState({ showLoader: true });
        const users = await getUsers();
        let ManagedUsers = [];
        users.data.map((data, index) => {
            let name = data.fname + " " + data.lname;
            let user = {
                key: index + 1,
                id: data._id,
                name: name,
                fname: data.fname,
                lname: data.lname,
                mo: data.mo,
                email: data.email,
                verified: data.verified,
            }
            ManagedUsers.push(user);

            return null;
        });
        this.setState({ users: ManagedUsers, showLoader: false });
    }

    render() {
        return (
            <>
                <Modal
                    title="Basic Modal"
                    visible={this.state.isModalVisible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="back" onClick={this.handleCancel}>
                        Cancel
                        </Button>,
                        <Button key="submit" loading={this.state.loading} type="primary" onClick={this.handleOk}>
                        Update
                        </Button>,
                    ]}
                >
                    <form>
						<div className="form-group">
							<label htmlFor="modalName">Name :</label>
							<input type="text" className="form-control" value={this.state.modalName}  onChange={this.handleChange} name="modalName" id="modalName" placeholder="Enter Name" />
						</div>
						<div className="form-group">
							<label htmlFor="modalLname">Lastname :</label>
							<input type="text" className="form-control" value={this.state.modalLname} onChange={this.handleChange} name="modalLname" id="modalLname" placeholder="Enter Lastname" />
						</div>
						<div className="form-group">
							<label htmlFor="modalMo">Mobile :</label>
							<input type="text" className="form-control" value={this.state.modalMo} onChange={this.handleChange} name="modalMo" id="modalMo" placeholder="Mobile Number" />
						</div>
						<div className="form-group">
							<label htmlFor="modalEmail">Email :</label>
							<input type="text" className="form-control" value={this.state.modalEmail} onChange={this.handleChange} name="modalEmail" id="modalEmail" placeholder="Email Id" />
						</div>
                    </form>
                </Modal>
                <div>
                    <Table columns={this.columns} dataSource={this.state.users} />
                </div>
                <Loader
                    showLoader={this.state.showLoader}
                />
            </>
        );
    }
}

export default Dashboard;