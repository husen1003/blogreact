import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { getBlogs, deleteBlog } from '../api';
// import { Popconfirm } from 'antd';
import { toast } from 'react-toastify';
import Loader from './loader';

class UpdateAndDelete extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			blogs: [],
			showLoader: false
		};
	}

	componentDidMount() {
		this.getData();
	}

	getData = async () =>{
		this.setState({showLoader: true});
		const res = await getBlogs();
		this.setState({blogs: res.data, showLoader: false});
	}

	render() {
		if(!this.props.isLoggedIn) {
			return <Redirect to="/user/login" />
		}
		return(
			<React.Fragment>
				{
					this.state.blogs.map((blog, id) => {
						return(
							<div key={id} className="jumbotron jumbotron-fluid" style={{background: '#e6ffe6'}} >
							  <div className="container">
							    <h1 className="display-4">{blog.title}</h1><br />
							    <p className="lead">{blog.desc}</p>
							  </div>
							<Link to={`/user/updateblog/${blog._id}`} type="button" className="container btn btn-secondary btn-lg btn-block">Edit Blog</Link>
							{/* <Popconfirm
								title="Are you sure to delete this Blog?"
								onConfirm={async () => {
									this.setState({showLoader: true});
									await deleteBlog(blog._id);
									const res = await getBlogs();
									this.setState({blogs: res.data, showLoader: false});
									toast.error('Blog Deleted!');
								}}
								okText="Delete"
								cancelText="No"
							> */}
								<Link to="#" type="button" onClick={async () => {
									this.setState({showLoader: true});
									await deleteBlog(blog._id);
									const res = await getBlogs();
									this.setState({blogs: res.data, showLoader: false});
									toast.error('Blog Deleted!');
								}} className="container btn btn-danger btn-lg btn-block">Delete Blog</Link>
							{/* </Popconfirm> */}
							</div>
						)
					})
				}
				<Loader
				showLoader={this.state.showLoader}
				/>	
			</React.Fragment>
		)
	}
}

const mapStateToProps = state => ({
	isLoggedIn: state.isLoggedIn
})

export default connect(mapStateToProps)(UpdateAndDelete);