import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { notLoggedIn } from '../redux/actions/actions'
import { toast } from 'react-toastify';
import Loader from './loader';

class Nav extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showLoader: false
		};
	}
	render() {
		return(
			<React.Fragment>
{			(this.props.isLoggedIn) ?
			<ul className="nav nav-pills" style={{background: "#99ff99", color: "white"}}>
				<li className="nav-item">
					<Link className="nav-link" style={{color: "black", border:"1px solid green"}} to="/user">Home</Link>
				</li>
				<li className="nav-item">
					<Link className="nav-link" style={{color: "black", border:"1px solid green"}} to="/user/addblog">Add Blog</Link>
				</li>
				<li className="nav-item">
					<Link className="nav-link" style={{color: "black", border:"1px solid green"}} to="/user/update-delete">Update Blog</Link>
				</li>
				<li className="nav-item">
					<Link className="nav-link" style={{color: "black", border:"1px solid green"}} to="/user/profile">Profile</Link>
				</li>
				<li className="nav-item">
					<Link className="nav-link" style={{color: "black", border:"1px solid green"}} onClick={() => {
						this.setState({showLoader: true});
						localStorage.removeItem("token");
						this.props.isNotLogin();
						toast.warn('Loggedout Successfully!');
						setTimeout(() => {
							this.setState({showLoader: false});
						}, 500);
					}} to="#">LogOut</Link>
				</li>
			</ul>
			:
			<ul className="nav nav-pills" style={{background: "#99ff99", color: "white"}}>
				<li className="nav-item">
					<Link className="nav-link" style={{color: "black", border:"1px solid green"}} to="/user/registration">Registration</Link>
				</li>
				<li className="nav-item">
					<Link className="nav-link" style={{color: "black", border:"1px solid green"}} to="/user/login">Login</Link>
				</li>
			</ul>
}
			<Loader
				showLoader={this.state.showLoader}				
			/>
			</React.Fragment>
		)
	}
}

const mapStateToProps = state => ({
	isLoggedIn: state.isLoggedIn
})

const mapDispatchToProps = (dispatch) => {
    return {
      isNotLogin: () => {dispatch(notLoggedIn())}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Nav);