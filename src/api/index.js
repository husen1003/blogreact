import axios from 'axios';

const BASE_URL = 'https://mern001.herokuapp.com/';
// const BASE_URL = 'http://localhost:5000/';

export const getUsers = data => {
	return axios.post(`${BASE_URL}api/getusers`, data).then(e => e);
}

export const newUser = data => {
	return axios.post(`${BASE_URL}api/newuser/`, data).then(e => e);
}

export const userLogin = data => {
	return axios.post(`${BASE_URL}api/login/`, data).then(e => e);
}

export const userVerify = token => {
	return axios.post(`${BASE_URL}api/userverify/`, token).then(e => e);
}

export const decodeToken = token => {
	return axios.post(`${BASE_URL}api/decodetoken/`, token).then(e => e);
}

export const userAuth = token => {
	return axios.post(`${BASE_URL}api/auth/`, token).then(e => e);
}

export const forgotPass = email => {
	return axios.post(`${BASE_URL}api/forgotpass`, email).then(e => e);
}

export const resetPass = data => {
	return axios.post(`${BASE_URL}api/resetpass`, data).then(e => e);
}

export const changeVerified = data => {
	return axios.post(`${BASE_URL}api/changeverified`, data).then(e => e);
}

export const newBlog = data => {
	return axios.post(`${BASE_URL}api/newblog/`, data).then(e => e);
}

export const getBlogs = () => {
	return axios.get(`${BASE_URL}api/getblogs/`).then(e => e);
}

export const deleteBlog = id => {
	return axios.delete(`${BASE_URL}api/deleteblog/${id}`).then(e => e);
}

export const getBlog = id => {
	return axios.get(`${BASE_URL}api/blog/${id}`).then(e => e);
}

export const editBlog = (data, id) => {
	return axios.put(`${BASE_URL}api/updateblog/${id}`,data).then(e => e);
}


// export const createProduct = data => {
// 	return axios.post(`${ BASEURL }api/product`, data).then(e => e);
// };