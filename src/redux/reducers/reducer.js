const initialState = {
	isLoggedIn: false,
	isAdminLoggedIn: false,
	counter: 0,
	users: [
		{
		  key: '1',
		  name: 'Husen Lokhandwala',
		  age: 22,
		  address: 'New York No. 1 Lake Park',
		},
	]
}

const reducer = (state=initialState, action) => {
	if(action.type === 'CHANGE_LOGIN_STATUS') {
		return {
			...state,
			isLoggedIn: true,
			user: action.payload
		}
	}
	if(action.type === 'IS_LOGGEDIN') {
		return {
			...state,
			isLoggedIn: true,
			user: action.payload
		}
	}
	if(action.type === 'NOT_LOGGEDIN') {
		return {
			...state,
			isLoggedIn: false
		}
	}
	if(action.type === 'INC') {
		let { counter } = state;
		counter++
		return {
			...state,
			counter: counter
		}
	}
	if(action.type === 'DEC') {
		let { counter } = state;
		counter--
		return {
			...state,
			counter: counter
		}
	}
	if(action.type === 'ADD_USER') {
		let { users } = state;
		return {
			...state,
			users: [...users, action.payload]
		}
	}
	if(action.type === 'EDIT_USER') {
		let { users } = state;
		let tmpArray = users;
		tmpArray[action.payload.id] = action.payload.data;
		console.log(users);
		return {
			...state,
			users: tmpArray,
		}
	}
	if(action.type === 'DELETE_USER') {
		let { users } = state;
		users.splice(action.payload, 1);
		return {
			...state,
			users: users,
		}
	}
	if(action.type === 'ADMIN_LOGIN') {
		return{
			...state,
			isAdminLoggedIn: true,
		}
	}
	if(action.type === 'ADMIN_LOGOUT') {
		return{
			...state,
			isAdminLoggedIn: false,
		}
	}
	
	return state;
}

export default reducer;