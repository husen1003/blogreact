export const changeLogin = data => {
	return {
		type: 'CHANGE_LOGIN_STATUS',
		payload: data
	}
}

export const isLogin = (data) => {
	return {
		type: 'IS_LOGGEDIN',
		payload: data
	}
}

export const notLoggedIn = () => {
	return {
		type: 'NOT_LOGGEDIN'
	}
}

export const adminLogin = data => {
	return {
		type: 'ADMIN_LOGIN',
		payload: data
	}
}

export const adminLogout = () => {
	return {
		type: 'ADMIN_LOGOUT',
	}
}

export const increment = () => {
	return {
		type: 'INC'
	}
}

export const decrement = () => {
	return {
		type: 'DEC'
	}
}

export const addNewUser = data => {
	return {
		type: 'ADD_USER',
		payload: data
	}
}

export const editUser = data => {
	return {
		type: 'EDIT_USER',
		payload: data
	}
}

export const deleteUser = id => {
	return {
		type: 'DELETE_USER',
		payload: id
	}
}