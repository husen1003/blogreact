import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Home from './components/home';
import Addblog from './components/addBlog';
import Updateblog from './components/updateBlog';
import Register from './components/users/Register';
import ForgotPassword from './components/users/ForgotPassword';
import ResetPassword from './components/users/ResetPassword';
import Login from './components/users/Login';
import Profile from './components/users/Profile';
import Count from './components/count'
import Nav from './components/nav';
import UpdateandDelete from './components/updateAndDelete';
import { userAuth } from './api';
import NotFound from './components/pageNotFound';
import { connect } from 'react-redux';
import { isLogin, notLoggedIn } from './redux/actions/actions'
import { toast } from 'react-toastify';
import 'antd/dist/antd.css';
import 'react-toastify/dist/ReactToastify.css';

//Importing Admin modules
import HeaderAdmin from './components/Admin/Header'
import Dashboard from './components/Admin/Dashboard';
import Blogs from './components/Admin/Blogs';

toast.configure()
function App(props) {

    const checkLoggedIn = async () => {
        const res = await userAuth({token: localStorage.getItem('token')});
        (!res.data.success) ? localStorage.removeItem('token') : localStorage.setItem('token', res.data.token)
        if(localStorage.getItem('token')) {
            props.isLogin({token: res.data.token, user: res.data.user});
        }
    }

    if(!localStorage.getItem('token')){
        props.isNotLogin();
    }
    else{
        checkLoggedIn();
    }

    return (
        <React.Fragment>
            <Switch>                            
                <Route
                    path="/user"
                    render={({ match: { url } }) => (
                        <>
                        <Nav />
                        <Route exact path={`${url}/`} component={Home} />
                        <Route exact path={`${url}/addblog`} component={Addblog} />
                        <Route exact path={`${url}/updateblog/:id`} component={Updateblog} />
                        <Route exact path={`${url}/update-delete`} component={UpdateandDelete} />
                        <Route exact path={`${url}/registration`} component={Register} />
                        <Route exact path={`${url}/login`} component={Login} />
                        <Route exact path={`${url}/login/:token`} component={Login} />
                        <Route exact path={`${url}/counter`} component={Count} />
                        <Route exact path={`${url}/forgotpassword`} component={ForgotPassword} />
                        <Route exact path={`${url}/forgotpassword/:email`} component={ForgotPassword} />
                        <Route exact path={`${url}/resetpassword/:token`} component={ResetPassword} />
                        <Route exact path={`${url}/profile`} component={Profile} />
                        </>
                    )}
                />                            
                <Route
                    path="/admin"
                    render={({ match: { url } }) => (
                        <>
                            <HeaderAdmin />
                            <Route exact path={`${url}/`} component={Dashboard} />
                            <Route exact path={`${url}/blogs`} component={Blogs} />
                        </>
                    )}
                />                

                {/* <Route exact path='/' component={Home} />
                <Route exact path='/addblog' component={Addblog} />
                <Route exact path='/updateblog/:id' component={Updateblog} />
                <Route exact path='/update-delete' component={UpdateandDelete} />
                <Route exact path='/registration' component={Register} />
                <Route exact path='/login' component={Login} />
                <Route exact path='/login/:token' component={Login} />
                <Route exact path='/counter/' component={Count} />
                <Route exact path='/forgotpassword/' component={ForgotPassword} />
                <Route exact path='/forgotpassword/:email' component={ForgotPassword} />
                <Route exact path='/resetpassword/:token' component={ResetPassword} />
                <Route exact path='/profile' component={Profile} />
                <Route exact path="/admin" component={Dashboard} /> */}

                <Redirect to="/user" />
                <Route component={NotFound} />

            </Switch>
        </React.Fragment>
    );
    }

    const mapStateToProps = state => ({
        isLoggendIn: state.isLoggendIn
    })

    const mapDispatchToProps = (dispatch) => {
        return {
            isLogin: data => {dispatch(isLogin(data))},
            isNotLogin: () => {dispatch(notLoggedIn())}
        }
    }

export default connect(mapStateToProps, mapDispatchToProps)(App);
